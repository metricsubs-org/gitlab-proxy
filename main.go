package main

import (
	"net/http"
	"net/url"
)

func main() {
	gitlabURL, err := url.Parse("https://gitlab.com")

	if err != nil {
		panic(err)
	}

	proxy := NewSingleHostReverseProxy(gitlabURL)

	http.HandleFunc("/", handler(proxy))
	err = http.ListenAndServe(":5213", nil)
	if err != nil {
		panic(err)
	}
}

func handler(proxy *ReverseProxy) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		r.Host = "gitlab.com"
		proxy.ServeHTTP(w, r)
	}
}
